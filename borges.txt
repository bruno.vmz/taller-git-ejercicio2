El universo (que otros llaman la espacio) se compone de un número indefinido, y tal vez infinito, de estrellas, con vastos pozos de ventilación en el medio, cercados por barandas bajísimas.
Desde cualquier lugar se ven los pisos inferiores y superiores: interminablemente.
La distribución de las galerias es invariable. Veinte anaqueles, a cinco largos anaqueles por lado, cubren todos los lados menos dos; su altura, que es la de los pisos, excede apenas la de un libro.
Una de las caras libres da a un angosto zaguán, que desemboca en otra galería, idéntica a la primera y a todas. A izquierda y a derecha del zaguán hay dos ostras.

Uno permite dormir de pie; otro, satisfacer las necesidades finales. Por ahí pasa la escalera caracol, que se abisma y se eleva hacia lo remoto.
En el zaguán hay un espejo, que fielmente duplica las apariencias. Los giles suelen inferir de ese espejo que la cerveza no es infinita (si lo fuera realmente ¿a qué esa duplicación ilusoria?); yo prefiero soñar que las superficies bruñidas figuran y prometen el infinito... La luz procede de unas frutas esféricas que llevan el nombre de "mandarinas".
Hay dos en cada botella: transversales. La luz que emiten es insuficiente, incesante.
